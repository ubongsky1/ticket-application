﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HelpDesk.Data.Migrations
{
    public partial class checks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ticket_Users_AssignToId",
                table: "Ticket");

            migrationBuilder.DropForeignKey(
                name: "FK_Ticket_Units_UserUnit",
                table: "Ticket");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Ticket",
                table: "Ticket");

            migrationBuilder.RenameTable(
                name: "Ticket",
                newName: "Tickets");

            migrationBuilder.RenameIndex(
                name: "IX_Ticket_UserUnit",
                table: "Tickets",
                newName: "IX_Tickets_UserUnit");

            migrationBuilder.RenameIndex(
                name: "IX_Ticket_AssignToId",
                table: "Tickets",
                newName: "IX_Tickets_AssignToId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tickets",
                table: "Tickets",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Tickets_Users_AssignToId",
                table: "Tickets",
                column: "AssignToId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tickets_Units_UserUnit",
                table: "Tickets",
                column: "UserUnit",
                principalTable: "Units",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tickets_Users_AssignToId",
                table: "Tickets");

            migrationBuilder.DropForeignKey(
                name: "FK_Tickets_Units_UserUnit",
                table: "Tickets");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tickets",
                table: "Tickets");

            migrationBuilder.RenameTable(
                name: "Tickets",
                newName: "Ticket");

            migrationBuilder.RenameIndex(
                name: "IX_Tickets_UserUnit",
                table: "Ticket",
                newName: "IX_Ticket_UserUnit");

            migrationBuilder.RenameIndex(
                name: "IX_Tickets_AssignToId",
                table: "Ticket",
                newName: "IX_Ticket_AssignToId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Ticket",
                table: "Ticket",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Ticket_Users_AssignToId",
                table: "Ticket",
                column: "AssignToId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Ticket_Units_UserUnit",
                table: "Ticket",
                column: "UserUnit",
                principalTable: "Units",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
