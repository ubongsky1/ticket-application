﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HelpDesk.Data.Models
{
    public class ApplicationUser : IdentityUser<int>
    {
        public ApplicationUser():base()
        {

        }
        [Required]
        [Display(Name = "Employee Firstname")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Employee Lastname")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Company Name")]
        public string Companyname { get; set; }

        public string Uname {
            get
            {
                return FirstName +"@helpdesk.com";
            }
        }
        [DataType(DataType.Date)]
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;


        public virtual ICollection<UserToken> UserTokens { get; set; }

        public virtual ICollection<UserRole> Roles { get; set; }

        public virtual ICollection<UserLogin> Logins { get; set; }

        public virtual ICollection<UserClaim> Claims { get; set; }
    }
}
