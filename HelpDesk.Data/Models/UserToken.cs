﻿using Microsoft.AspNetCore.Identity;



namespace HelpDesk.Data.Models
{
    public class UserToken : IdentityUserToken<int>
    {
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}