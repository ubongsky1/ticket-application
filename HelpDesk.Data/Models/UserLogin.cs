﻿using Microsoft.AspNetCore.Identity;

namespace HelpDesk.Data.Models
{
    public class UserLogin :IdentityUserLogin<int>
    {
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}