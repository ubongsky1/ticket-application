﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpDesk.Data.Models
{
   public static class SD
    {
        public const string SuperAdmin = "Super Admin";
        public const string Admin = "Admin";
        public const string User = "Users";
        public const string HRHead = "HR Head";
        public const string HR = "HR";
        public const string UnitHead = "Unit Head";
        public const string DeptHead = "Dept Head";
        public const string RegionalHead = "Regional Head";
    }
}
