﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HelpDesk.Data.Models
{
   public class Ticket
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Content { get; set; }
        public string Status { get; set; }
        public string Response { get; set; }
        public string Priority { get; set; }
        public string TreatedBy { get; set; }
        public bool IsDeleted { get; set; }
        [Display(Name = "Treated")]
        public bool isTreated { get; set; }
        [Display(Name = "Un-treated")]
        public bool notTreated { get; set; }
        [DisplayName("Added By")]
        public string AddedBy { get; set; }
        [Display(Name = "Assign To")]
        [ForeignKey("ApplicationUser")]
        public int AssignToId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        [Display(Name = "Unit")]
        [ForeignKey("Units")]
        public int UserUnit { get; set; }
        public Units Units { get; set; }
        [DisplayName("Added On")]
        public DateTime CreateDate { get; set; } = DateTime.Now;
    }
}
