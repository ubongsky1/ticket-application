﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(document).ready(function () {
    $.ajaxSetup({ cache: false });
    $.getJSON("/Tickets/GetData", function (data) {

        Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Tickets Report'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name: 'Tickets',
                colorByPoint: true,
                data: [{
                    name: 'All',
                    y: data.Total
                }, {
                    name: 'Treated',
                    y: data.Approved
                }, {
                    name: 'Untreated',
                    y: data.Disapproved
                    }, {
                        name: 'Pending',
                        y: data.Pending
                    }]
            }]
        });

    });
    $('#customerDatatable').dataTable({
     
    });

    $('#customerDatatables').dataTable({
        dom: 'lBfrtip',
        buttons: ['excel', 'pdf', 'print',]
        
    });

});


//$(document).ready(function () {
//    $.ajaxSetup({ cache: false });
//    $('#customerDatatable').dataTable({
//        dom: 'Bfrtip',
//        buttons: [
//            'copy', 'csv', 'excel', 'pdf', 'print'
//        ]
//    });


//});

function RenderActions(RenderActionstring) {
    $("#OpenDialog").load(RenderActionstring);
};

function CreateNew() {
    if (!ValidateInput())
        return;
    $("#loader").show();

    $.ajax({
        url: '/Tickets/Create/',
        type: 'POST',
        data: $("#some").serialize(),
        success: function () {
            $("#loader").hide();
           
            swal("Success", "Ticket Created", "success");
            window.location.reload();
        }
    })
};

function EditEmp(id) {
    $("#loader").show();
    $.ajax({
        url: '/Tickets/Edit/' + id,
        type: 'POST',
        data: $('#EditForm').serialize(),
        success: function () {
            $("#loader").hide();
            $("#modalCreate").modal("hide");
            swal("Success", "Ticket Edited", "success");
            window.location.reload();
        }

    })
};

function ValidateInput() {
    var flag = true;
    var firstNameInput = $('#subject');
    var secondNameInput = $('#content');


    if ($.trim(firstNameInput.val()) != '') {
        firstNameInput.closest('.form-group').find('span').hide();
        flag = true;
    }

    if ($.trim(secondNameInput.val()) != '') {
        secondNameInput.closest('.form-group').find('span').hide();
        flag = true;
    }

    if ($.trim(firstNameInput.val()) === '') {
        firstNameInput.closest('.form-group').find('span').text("This field is required");
        flag = false;
    }

    if ($.trim(secondNameInput.val()) === '') {
        secondNameInput.closest('.form-group').find('span').text("This field is required");
        flag = false;
    }

    return flag;
};



